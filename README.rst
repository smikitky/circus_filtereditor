CIRCUS Filter Editor
====================

This is a jQuery UI plugin which enables you to visually build complex
conditionals such as ``((A > 100 OR B <= 200) AND C != 150) OR D = 50``.

Derived from CIRCUS CS as an independent project.

The result conditional can be exposed as:

- JSON object for CIRCUS CS filter
- MongoDB-style conditional

Requiments
----------

- jQuery, jQuery UI (with Button and Draggable support)
- IE >= 8 or any other modern browser

How to Use
----------

See the demo in the ``demo`` directory.

1. In your HTML file, load jQuery, jQuery UI.
2. Load jquery.ruleseteditor.js and jquery.ruleseteditor.css
3. Create ``div`` element, create jQuery object of that element,
   and call ``filtereditor`` method for it::

     var editor = $('#mydiv').filtereditor();

4. The editor triggers ``filterchange`` event whenever there is a meaningful
   change inside the editor. You can access the content of the editor
   as a plain JavaScript object using ``filter`` option::

     editor.on('filterchange', function() {
       var data = editor.filtereditor('option', 'filter');
       $('textarea#result').val(JSON.stringify(data));
     });

5. Additionally, MongoDB-style query condition can be exported, as follows::

     var mongo_where = editor.filtereditor('createMongoCondFromElement');

   Currently, this is a one-way feature. You cannot set back mong-style objects.

6. You can set the condition, as follows::

     var js_object = {
       group: "and",
       "members": [ { key: "modality", condition: "=", value: "CT" } ]
     };
     editor.filtereditor('option', 'filter', js_object);

Options
-------

``keys``
  The list of available keys. Currently only valid at initialization.
  Each key is represented as objects containing one or two properties, as follows::

    [ { value: "keyNameA", label: "label" }, { value: "keyNameB" }, ... ]

  Default: ``[ { value: 'name' }, { value: 'date' } ]``

``filter``
  Readable and writable. The current content of the editor, represented as
  plain JS object.

  Default: "AND" group containing only one dummyCondition.

``dummyCondition``
  Readable and writable. The default node which is inserted when "plus" button
  was clicked.

  Default: ``{ key: keys[0], condition: '=', value: '' }``, where ``keys[0]`` is
  the first key of the key list.

ToDo
----

- Support for "disabled" status
- Support for value type (number and string)
- Tests

License
-------

Modified BSD License.

Copyright 2014 UTRAD-ICAL.